import { useCallback, useEffect, useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import api from "../../services/api";
import {
  Container,
  Content,
  Data,
  FormContainer,
  CardsContainer,
  LogoutBtn,
} from "./styles";
import TechsForm from "../../components/TechsForm";
import Card from "../../components/Card";
import { IoLogOut } from "react-icons/io5";

const Dashboard = ({ authenticated, setAuthenticated }) => {
  const [user, setUser] = useState({});

  const [token] = useState(
    JSON.parse(localStorage.getItem("@kenziehub:token")) || ""
  );

  const history = useHistory();

  const loadUserData = useCallback(() => {
    api
      .get("/profile", { headers: { Authorization: `Bearer ${token}` } })
      .then((response) => {
        setUser(response.data);
      });
  }, [token]);

  const addTech = (data) => {
    api
      .post("/users/techs", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => loadUserData())
      .catch((err) => console.log(err));
  };

  const deleteTech = (IdValue) => {
    api
      .delete(`/users/techs/${IdValue}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((_) => loadUserData())
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    loadUserData();
  }, [loadUserData]);

  if (!authenticated) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      <h1>Dashboard</h1>
      <LogoutBtn
        onClick={() => {
          localStorage.clear();
          setAuthenticated(false);
          history.push("/");
        }}
      >
        Sair
        <IoLogOut />
      </LogoutBtn>
      <Content>
        <Data>
          <h2>{user.name}</h2>
          <span>{user.course_module}</span>
        </Data>
        <FormContainer>
          <h2>Adicionar tecnologias</h2>
          <TechsForm addTech={addTech} />
        </FormContainer>
        <CardsContainer aria-label="cards container">
          {user?.techs?.map((tech) => (
            <Card key={tech.id} tech={tech} deleteTech={deleteTech} />
          ))}
        </CardsContainer>
      </Content>
    </Container>
  );
};

export default Dashboard;
