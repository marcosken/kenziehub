import { render, screen } from "@testing-library/react";
import MockAdapter from "axios-mock-adapter";
import api from "../../services/api";
import Dashboard from ".";
import userEvent from "@testing-library/user-event";

const apiMock = new MockAdapter(api);

const responseWithEmptyTechList = {
  id: "a7bfcf22-8fb8-474b-aafa-14b389425733",
  name: "nick",
  email: "nick@mail.com",
  course_module: "Segundo módulo",
  bio: "oi",
  contact: "Telefone",
  techs: [],
  works: [],
  created_at: "2023-06-16T03:47:10.897Z",
  updated_at: "2023-06-16T03:47:10.897Z",
  avatar_url: null,
};

const responseWithOneTech = {
  id: "a7bfcf22-8fb8-474b-aafa-14b389425733",
  name: "nick",
  email: "nick@mail.com",
  course_module: "Segundo módulo",
  bio: "oi",
  contact: "Telefone",
  techs: [
    {
      id: "735f4865-2bc8-4ec1-8871-d572e8637fc7",
      title: "React",
      status: "Intermediário",
      created_at: "2023-06-26T21:56:43.852Z",
      updated_at: "2023-06-26T21:56:43.852Z",
    },
  ],
  works: [],
  created_at: "2023-06-16T03:47:10.897Z",
  updated_at: "2023-06-16T03:47:10.897Z",
  avatar_url: null,
};

const responseWithTwoTechs = {
  id: "a7bfcf22-8fb8-474b-aafa-14b389425733",
  name: "nick",
  email: "nick@mail.com",
  course_module: "Segundo módulo",
  bio: "oi",
  contact: "Telefone",
  techs: [
    {
      id: "735f4865-2bc8-4ec1-8871-d572e8637fc7",
      title: "React",
      status: "Intermediário",
      created_at: "2023-06-26T21:56:43.852Z",
      updated_at: "2023-06-26T21:56:43.852Z",
    },
    {
      id: "f56eee17-3e48-4747-b5fb-e3dbc10e445c",
      title: "Python",
      status: "Básico",
      created_at: "2023-06-26T23:22:13.901Z",
      updated_at: "2023-06-26T23:22:13.901Z",
    },
  ],
  works: [],
  created_at: "2023-06-16T03:47:10.897Z",
  updated_at: "2023-06-16T03:47:10.897Z",
  avatar_url: null,
};

describe("Dashboard Page", () => {
  test("should not show any technology card on first render if user has not registered any technology", async () => {
    apiMock.onGet("/profile").replyOnce(200, responseWithEmptyTechList);

    render(<Dashboard authenticated={true} setAuthenticated={() => {}} />);

    const cardContainer = await screen.findByRole("generic", {
      name: "cards container",
    });

    expect(cardContainer).toBeEmptyDOMElement();
  });

  test("should show the technology cards on first render if user has registered some technology", async () => {
    apiMock.onGet("/profile").replyOnce(200, responseWithOneTech);

    render(<Dashboard authenticated={true} setAuthenticated={() => {}} />);

    const cards = await screen.findByRole("generic", {
      name: "cards container",
    });

    expect(cards).toHaveTextContent("React");
  });

  test("should show a new technology card when user add new technology", async () => {
    apiMock
      .onGet("/profile")
      .replyOnce(200, responseWithOneTech)
      .onGet("/profile")
      .replyOnce(200, responseWithTwoTechs);

    apiMock.onPost("/users/techs").replyOnce(200, {
      id: "f56eee17-3e48-4747-b5fb-e3dbc10e445c",
      title: "Python",
      status: "Básico",
      user: {
        id: "a7bfcf22-8fb8-474b-aafa-14b389425733",
      },
      created_at: "2023-06-26T23:22:13.901Z",
      updated_at: "2023-06-26T23:22:13.901Z",
    });

    render(<Dashboard authenticated={true} />);

    const techTitleInput = screen.getByPlaceholderText("Tecnologia");
    const techStatusInput = screen.getByPlaceholderText(
      "Nível de conhecimento"
    );

    userEvent.type(techTitleInput, "Python");
    userEvent.type(techStatusInput, "Básico");

    const addButton = screen.getByRole("button", { name: /adicionar/i });

    userEvent.click(addButton);

    const cardContainer = await screen.findByRole("generic", {
      name: "cards container",
    });

    const cards = await screen.findAllByRole("generic", { name: "tech card" });

    expect(cardContainer).toHaveTextContent("React");
    expect(cardContainer).toHaveTextContent("Python");

    expect(cards).toHaveLength(2);
  });

  test("should not show the technology card when user delete this technology", async () => {
    apiMock
      .onGet("/profile")
      .replyOnce(200, responseWithOneTech)
      .onGet("/profile")
      .replyOnce(200, responseWithEmptyTechList);

    apiMock
      .onDelete("/users/techs/735f4865-2bc8-4ec1-8871-d572e8637fc7")
      .replyOnce(204, {});

    render(<Dashboard authenticated={true} />);

    const deleteButton = await screen.findByRole("button", {
      name: /remover/i,
    });

    userEvent.click(deleteButton);

    const cards = await screen.findByRole("generic", {
      name: "cards container",
    });

    expect(cards).toBeEmptyDOMElement();
  });
});
