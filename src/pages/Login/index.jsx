import { Container, Content, ImageSection } from "./styles";
import login from "../../assets/login.svg";
import Figure from "../../components/Figure";
import LoginForm from "../../components/LoginForm";
import { Redirect } from "react-router-dom";

const Login = ({ authenticated, setAuthenticated }) => {
  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Content>
        <div>
          <h1>KenzieHub</h1>
          <h2>Login</h2>
          <LoginForm setAuthenticated={setAuthenticated} />
        </div>
      </Content>
      <ImageSection>
        <Figure url={login} />
      </ImageSection>
    </Container>
  );
};

export default Login;
