import ComponentButton from "../../components/Button";
import Figure from "../../components/Figure";
import { Container, Content, ImageSection } from "./styles";
import home from "../../assets/home.png";
import { Redirect, useHistory } from "react-router-dom";

const Home = ({ authenticated }) => {
  const history = useHistory();

  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Content>
        <div>
          <h1>KenzieHub</h1>
          <span>
            O hub de portfólio dos programadores da Kenzie Academy Brasil!
          </span>
          <ComponentButton onClick={() => history.push("/signup")}>
            Quero me cadastrar!
          </ComponentButton>
          <ComponentButton onClick={() => history.push("/login")}>
            Já tenho cadastro
          </ComponentButton>
        </div>
      </Content>
      <ImageSection>
        <Figure url={home} />
      </ImageSection>
    </Container>
  );
};

export default Home;
