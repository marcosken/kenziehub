import { Container, Content, ImageSection } from "./styles";
import signup from "../../assets/signup.svg";
import Figure from "../../components/Figure";
import SignUpForm from "../../components/SignUpForm";
import { Redirect } from "react-router-dom";

const SignUp = ({ authenticated }) => {
  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <ImageSection>
        <Figure url={signup} />
      </ImageSection>
      <Content>
        <div>
          <h1>KenzieHub</h1>
          <h2>Criar conta</h2>
          <SignUpForm />
        </div>
      </Content>
    </Container>
  );
};

export default SignUp;
