import styled from "styled-components";

export const Container = styled.div`
  background: url(${(props) => props.url});
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
  height: 100%;
`;
