import styled from "styled-components";

export const Form = styled.form`
  span {
    font-size: 0.9rem;
    color: var(--red);
  }
`;
