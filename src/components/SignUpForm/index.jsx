import React from "react";
import ComponentInput from "../Input";
import ComponentButton from "../Button";
import { Form } from "./styles";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

const SignUpForm = () => {
  const history = useHistory();

  const schema = yup.object().shape({
    email: yup.string().required("Campo obrigatório").email("Email inválido"),
    password: yup
      .string()
      .min(6, "Mínimo de 6 dígitos")
      .required("Campo obrigatório"),
    name: yup.string().required("Campo obrigatório"),
    bio: yup.string().required("Campo obrigatório"),
    contact: yup.string().required("Campo obrigatório"),
    course_module: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmitFunction = (data) => {
    api
      .post("/users", data)
      .then((_) => {
        toast.success("Parabéns! Sua conta foi criada com sucesso!");
        return history.push("/login");
      })
      .catch((_) => toast.error("Erro ao criar a conta. Tente novamente."));
  };

  return (
    <Form onSubmit={handleSubmit(onSubmitFunction)}>
      <span>{errors.email?.message}</span>
      <ComponentInput
        placeholder="E-mail"
        register={register}
        inputName="email"
      />
      <span>{errors.password?.message}</span>
      <ComponentInput
        placeholder="Senha"
        register={register}
        inputName="password"
      />
      <span>{errors.name?.message}</span>
      <ComponentInput placeholder="Nome" register={register} inputName="name" />
      <span>{errors.bio?.message}</span>
      <ComponentInput
        placeholder="Sobre você"
        register={register}
        inputName="bio"
      />
      <span>{errors.contact?.message}</span>
      <ComponentInput
        placeholder="Contato"
        register={register}
        inputName="contact"
      />
      <span>{errors.course_module?.message}</span>
      <ComponentInput
        placeholder="Módulo do curso"
        register={register}
        inputName="course_module"
      />
      <ComponentButton type="submit">CADASTRAR</ComponentButton>
    </Form>
  );
};

export default SignUpForm;
