import { render, screen } from "@testing-library/react";
import ComponentButton from ".";
import userEvent from "@testing-library/user-event";

describe("Button Component", () => {
  test("should have blue background color by default", () => {
    render(<ComponentButton>Button Text</ComponentButton>);

    const buttonElement = screen.getByRole("button", { name: /button text/i });

    expect(buttonElement).toHaveStyle({ backgroundColor: "#1480fb" });
  });

  test("should have purple background color when purpleSchema property is true", () => {
    render(<ComponentButton purpleSchema>Button Text</ComponentButton>);

    const buttonElement = screen.getByRole("button", { name: /button text/i });

    expect(buttonElement).toHaveStyle({ backgroundColor: "#6d0c91" });
  });

  test("should call the onClick function when it is clicked", () => {
    const onClickMock = jest.fn();

    render(
      <ComponentButton onClick={onClickMock}>Button Text</ComponentButton>
    );

    const buttonElement = screen.getByRole("button", { name: /button text/i });

    userEvent.click(buttonElement);

    expect(onClickMock).toHaveBeenCalled();
  });
});
