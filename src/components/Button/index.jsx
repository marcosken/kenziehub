import { Button } from "./styles";

const ComponentButton = ({ children, purpleSchema, onClick, type }) => {
  return (
    <Button type={type} onClick={onClick} purpleSchema={purpleSchema}>
      {children}
    </Button>
  );
};

export default ComponentButton;
