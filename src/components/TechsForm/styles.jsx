import styled from "styled-components";

export const Form = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  span {
    display: block;
    font-size: 0.9rem;
    color: var(--red);
  }

  div:first-child {
    /* flex-grow: 2; */
    width: 70%;
    @media screen and (min-width: 1024px) {
      margin-right: 10px;
    }
  }

  div:last-child {
    width: 40%;
    @media screen and (min-width: 1024px) {
      width: 30%;
    }
  }

  button {
    font-size: 1.3rem;
    padding: 10px;
  }

  @media screen and (min-width: 1024px) {
    width: 70%;
    flex-direction: row;
  }
`;
