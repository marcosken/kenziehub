import { render, screen, wait, waitFor } from "@testing-library/react";
import TechsForm from ".";
import userEvent from "@testing-library/user-event";

const addTechMock = jest.fn();

describe("TechsForm component", () => {
  test("should not submit the form if the fields are empty", async () => {
    render(<TechsForm addTech={addTechMock} />);

    const addButton = screen.getByRole("button", {
      name: /adicionar/i,
    });

    userEvent.click(addButton);

    await waitFor(() => {
      expect(addTechMock).not.toHaveBeenCalled();

      expect(screen.getAllByText(/campo obrigatório/i)).toHaveLength(2);
    });
  });

  test("should clean field after the user submit the form", async () => {
    render(<TechsForm addTech={addTechMock} />);

    const techTitleInput = screen.getByPlaceholderText("Tecnologia");
    const techStatusInput = screen.getByPlaceholderText(
      "Nível de conhecimento"
    );
    const addButton = screen.getByRole("button", {
      name: /adicionar/i,
    });

    userEvent.type(techTitleInput, "Python");
    userEvent.type(techStatusInput, "Básico");
    userEvent.click(addButton);

    await waitFor(() => {
      expect(techTitleInput).not.toHaveTextContent();
      expect(techStatusInput).not.toHaveTextContent();
    });
  });
});
