import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import api from "../../services/api";
import MockAdapter from "axios-mock-adapter";
import LoginForm from ".";

const apiMock = new MockAdapter(api);

const mockHistoryPush = jest.fn();

jest.mock("react-router-dom", () => {
  return {
    ...jest.requireActual("react-router-dom"),
    useHistory: () => {
      return {
        push: mockHistoryPush,
      };
    },
  };
});

const getEmailField = () => {
  return screen.getByPlaceholderText("E-mail");
};

const getPasswordField = () => {
  return screen.getByPlaceholderText("Senha");
};

const getSubmitButton = () => {
  return screen.getByRole("button", {
    name: /entrar/i,
  });
};

test("should be able to sign in", async () => {
  apiMock.onPost("/sessions").replyOnce(200, {});

  render(<LoginForm setAuthenticated={jest.fn()} />);

  const emailField = getEmailField();
  const passwordField = getPasswordField();
  const submitButton = getSubmitButton();

  userEvent.type(emailField, "jhondoe@mail.com");
  userEvent.type(passwordField, "123456");

  userEvent.click(submitButton);

  await waitFor(() => {
    expect(emailField).toHaveValue("jhondoe@mail.com");
    expect(passwordField).toHaveValue("123456");

    expect(mockHistoryPush).toHaveBeenCalledWith("/dashboard");
  });
});

test("should not be able to sign in with invalid credentials", async () => {
  render(<LoginForm setAuthenticated={jest.fn()} />);

  const emailField = getEmailField();
  const passwordField = getPasswordField();
  const submitButton = getSubmitButton();

  userEvent.type(emailField, "jhondoe@mail");
  userEvent.type(passwordField, "1234");

  userEvent.click(submitButton);

  await waitFor(() => {
    expect(emailField).toHaveValue("jhondoe@mail");
    expect(passwordField).toHaveValue("1234");

    expect(screen.getByText(/email inválido/i)).toBeInTheDocument();
    expect(screen.getByText(/mínimo de 6 dígitos/i)).toBeInTheDocument();

    expect(mockHistoryPush).not.toHaveBeenCalledWith("/dashboard");
  });
});
