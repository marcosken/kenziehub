import React from "react";
import { Input } from "./styles";

const ComponentInput = ({ placeholder, register, inputName }) => {
  return (
    <Input
      placeholder={placeholder}
      {...register(inputName)}
      type={(inputName = "password" && inputName)}
    ></Input>
  );
};

export default ComponentInput;
